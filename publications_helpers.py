#!/usr/bin/env python3
# coding: utf-8
# Helper functions for formatting publications page

import os
import re
import html
import json
from PIL import Image
from collections import OrderedDict
from pybtex.database import BibliographyData, Entry, parse_file
import pybtex.database.output.bibtex
from pybtex.richtext import Text
from pybtex.database import Person


def customAlphaSort(list):
    # this function is needed only to ensure that a file like 'paper.pdf' appears before 'paper-supplemental.pdf'
    # all I really wanted to do was swap the lexographical order of '.' and '-'.
    # There might be a better way.
    alphabet = {'.': 0, '-': 1}
    return sorted(list, key=lambda word: [alphabet.get(c, ord(c)) for c in word])


def key_by_year(x):
    month = int(x[1].fields.get('month', 0))
    year = int(x[1].fields.get('year', 0))
    return year + (month/12.0)


def latex_to_unicode(s):
    return Text.from_latex(s).render_as('plaintext')


def personName(person, lastname_first=False, abbr=False):
    from pybtex.textutils import abbreviate
    firstandmiddlenames = abbreviate(' '.join(person.first_names + person.middle_names)) if abbr else ' '.join(person.first_names + person.middle_names)
    if lastname_first:
        name = ' '.join(person.prelast_names + person.last_names + person.lineage_names) + ', ' + firstandmiddlenames
    else:
        name = firstandmiddlenames + ' ' + ' '.join(person.prelast_names + person.last_names + person.lineage_names)
    name = latex_to_unicode(name)
    return name


def addPerson(person, affiliation, author_websites):
    author = {}
    author["name"] = personName(person)
    if author["name"] in author_websites:
        author["url"] = author_websites[author["name"]]
    author["affiliation"] = affiliation
    return author


def extractCollaborators(bib_sorted, year_range, lastname_first=False):
    collaboratorSet = set()
    gen = ((key, bibentry) for key, bibentry in bib_sorted if "extra-noconflicts" not in bibentry.fields and bibentry.type != "patent" and int(bibentry.fields[u'year']) in year_range and 'author' in bibentry.persons)
    for key, bibentry in gen:
        for person in bibentry.persons['author']:
            collaboratorSet.add(personName(person, lastname_first))

    return collaboratorSet


def formatCollaborators(collaboratorSet, author_websites, lastname_first=False, abbr=False):
    collaborators = []
    myname = u'Wojciech Jarosz'
    for name in sorted(collaboratorSet):
        if name == myname:
            continue
        key = personName(Person(name))
        displayname = personName(Person(name), lastname_first, abbr)
        print([key, displayname])
        if key in author_websites:
            collaborators.append(u"""<a href="%s">%s</a>""" % (author_websites[key], displayname))
        else:
            collaborators.append(displayname)

    return collaborators


def writeEntryAsBibTex(entry, key, include_abstract=True):
    # remove the abstract and "extra" fields, but preserve field order
    newfields = OrderedDict((key, value) for key, value in entry.fields.items() if "extra" not in key and "author+an" not in key and (include_abstract or key != "abstract"))

    writer = pybtex.database.output.bibtex.Writer(encoding="utf-8")
    s = writer.to_string(BibliographyData(entries={key: Entry(type_=entry.type, fields=newfields, persons=entry.persons)}))

    # remove quotes from 3-letter month macros
    s = s.replace('\"jan\"', 'jan')
    s = s.replace('\"feb\"', 'feb')
    s = s.replace('\"mar\"', 'mar')
    s = s.replace('\"apr\"', 'apr')
    s = s.replace('\"may\"', 'may')
    s = s.replace('\"jun\"', 'jun')
    s = s.replace('\"jul\"', 'jul')
    s = s.replace('\"aug\"', 'aug')
    s = s.replace('\"sep\"', 'sep')
    s = s.replace('\"oct\"', 'oct')
    s = s.replace('\"nov\"', 'nov')
    s = s.replace('\"dec\"', 'dec')
    return s

def size_to_string(size):
    if size < 1024:
        return u'%dB' % size
    elif size < 1024 * 1024:
        return u'%dKB' % (size * .0009765625)
    elif size < 1024 * 1024 * 1024:
        return u'%dMB' % (size * .0009765625 * .0009765625)
    elif size < 1024 * 1024 * 1024 * 1024:
        return u'%dGB' % (size * .0009765625 * .0009765625 * .0009765625)

def addBasicFields(paper, base_filename, bibentry):
    paper["base_filename"] = base_filename
    paper["type"] = bibentry.type
    title = Text.from_latex(bibentry.fields['title'])

    if not bibentry.type in ['proceedings', 'phdthesis', 'thesis', 'book']:
        title = title.capitalize()

    paper["title"] = title.render_as('plaintext')
    paper["venue"] = getVenue(bibentry)

# copy fields from bib entry to jinja
def addRemainingBibFields(paper, bibentry):
    # convert 3-letter month acronyms to full English month names in text citations
    short_to_full_month_names = {
        'jan': 'January',
        'feb': 'February',
        'mar': 'March',
        'apr': 'April',
        'may': 'May',
        'jun': 'June',
        'jul': 'July',
        'aug': 'August',
        'sep': 'September',
        'oct': 'October',
        'nov': 'November',
        'dec': 'December'
    }
    for field in ['number', 'volume', 'pages', 'year', 'abstract', 'pubstate', 'note']:
        if field in bibentry.fields:
            paper[field] = latex_to_unicode(bibentry.fields[field])
    if 'month' in bibentry.fields:
        paper['month'] = latex_to_unicode(short_to_full_month_names[bibentry.fields['month']])
    for field in ['select', 'accolades', 'accoladesurl', 'copyright', 'caption']:
        extrafield = 'extra-' + field
        if extrafield in bibentry.fields:
            paper[field] = latex_to_unicode(bibentry.fields[extrafield])

def addTeaserAndThumbnail(paper, json_data, base_filename, bibentry):
    # Find the thumbnail and teaser images
    m = re.compile(base_filename + json_data["regular-expressions"]["teaser"])
    for filename in customAlphaSort(os.listdir('images/')):
        g = m.search(filename)
        if g:
            paper["teaser_image"] = 'images/' + filename

    m = re.compile(base_filename + json_data["regular-expressions"]["thumbnail"])
    for filename in customAlphaSort(os.listdir('images/')):
        g = m.search(filename)
        if g:
            paper["thumbnail"] = 'images/' + filename

    teaser_width = "900"
    if 'extra-caption-width' in bibentry.fields:
        teaser_width = str(bibentry.fields['extra-caption-width'])
    elif "teaser_image" in paper:
        try:
            img = Image.open(paper["teaser_image"])
            # get the image's width and height in pixels
            width, height = img.size
            teaser_width = str(width)
        except:
            # filename not an image file
            print(u"Error reading teaser image: %s" % paper["teaser_image"])

    paper["teaser_width"] = teaser_width


def addProjectURL(paper, base_filename, bibentry):
    if bibentry.type == 'patent':
        paper["url"] = html.escape(bibentry.fields['url'], quote=True)
        paper["external"] = True
    elif 'extra-webpage' in bibentry.fields:
        paper["url"] = html.escape(bibentry.fields['extra-webpage'], quote=True)
        paper["external"] = 'http://' in bibentry.fields['extra-webpage'] or 'https://' in bibentry.fields['extra-webpage']
    elif 'extra-no-webpage' not in bibentry.fields:
        paper["url"] = u'%s.html' % base_filename


def getVenue(bibentry):
    if bibentry.type == 'article':
        return latex_to_unicode(bibentry.fields['journal'])
    elif bibentry.type == 'inproceedings':
        return latex_to_unicode(bibentry.fields['booktitle'])
    elif bibentry.type == 'proceedings':
        return latex_to_unicode(bibentry.fields['series'])
    elif bibentry.type == 'phdthesis':
        return u'Ph.D. Dissertation, %s' % latex_to_unicode(bibentry.fields['school'])
    elif bibentry.type == 'thesis':
        return u'%s, %s' % (latex_to_unicode(bibentry.fields['type']), latex_to_unicode(bibentry.fields['school']))
    elif bibentry.type == 'techreport':
        return u'Tech. Report, %s' % latex_to_unicode(bibentry.fields['institution'])
    elif bibentry.type == 'patent':
        return u'Patent, %s %s' % (latex_to_unicode(bibentry.fields['location']), latex_to_unicode(bibentry.fields['number']))
    elif bibentry.type == 'misc':
        return latex_to_unicode(bibentry.fields['howpublished'])


def addBasicDownloads(links, dtype, json_data, base_filename, quick=False):
    # print(json_data["regular-expressions"][dtype])
    m = re.compile(base_filename + json_data["regular-expressions"][dtype])
    # print('\n'.join(os.listdir('./')))
    for filename in customAlphaSort(os.listdir('./')):
        g = m.search(filename)
        if g:
            suffix = g.group(1)
            num = g.group(2)
            ext = g.group(3)
            if not quick or ("-small" not in suffix and "-fast-forward" not in suffix and ext not in ['mov', 'mp4', 'm4v', 'webm']):
                links.append(basicDownload(dtype, filename, json_data, num, suffix, ext))


def basicDownload(dtype, filename, json_data, num, suffix, ext):
    type_short = json_data[dtype + "-suffix-descriptions"][suffix]["short"]
    type_long = json_data[dtype + "-suffix-descriptions"][suffix]["long"]
    if num != '':
        type_long += u' ' + num
        type_short += u' ' + num

    link = {}
    link["type"] = ext
    link["url"] = filename
    link["text_short"] = u'%s (%s %s)' % (type_short, size_to_string(os.path.getsize(filename)), ext)
    link["text_long"] = type_long#, json_data["extension-descriptions"][ext], size_to_string(os.path.getsize(filename)))
    link["size"] = size_to_string(os.path.getsize(filename))
    link["extension_description"] = json_data["extension-descriptions"][ext]
    return link


def addDOILink(links, bibentry):
    # add ACM Author-Izer link if it exists (ACM updated how they expose these links, so we have two versions)
    if 'extra-acm-author-izer-v2' in bibentry.fields:
        id = html.escape(bibentry.fields['extra-acm-author-izer-v2'], quote=True)
        link = {}
        link["type"] = u'acm'
        link["url"] = u'https://dl.acm.org/doi/%s' % id
        link["external"] = True
        link["text_long"] = u'Official ACM Author-Izer version'
        link["text_short"] = u'publisher\'s version'
        links.append(link)
    # add ACM Author-Izer link if it exists (ACM updated how they expose these links, so we have two versions)
    elif 'extra-acm-author-izer' in bibentry.fields:
        id = html.escape(bibentry.fields['extra-acm-author-izer'], quote=True)
        link = {}
        link["type"] = u'acm'
        link["url"] = u'https://dl.acm.org/authorize?%s' % id
        link["external"] = True
        link["text_long"] = u'Official ACM Author-Izer version'
        link["text_short"] = u'publisher\'s version'
        links.append(link)
    # otherwise add official publisher's page from DOI link
    elif 'doi' in bibentry.fields:
        url = html.escape(bibentry.fields['doi'], quote=True)
        link = {}
        link["type"] = u'doi-link'
        link["url"] = u'https://doi.org/%s' % url
        link["external"] = True
        link["text_long"] = u'Publisher\'s official version (may require a subscription)'
        link["text_short"] = u'publisher\'s version'
        links.append(link)


def addExtraDownloads(links, type, bibentry):
    for datanumber in range(1, 20):
        key = "extra-" + type + "-download-" + str(datanumber)
        if key in bibentry.fields:
            link = {}
            link["link"] = latex_to_unicode(bibentry.fields[key])
            link["external"] = 'http://' in bibentry.fields[key] or 'https://' in bibentry.fields[key]

            # set the link type using either the explicit bib field, or leave as default for type
            typekey = "extra-" + type + "-download-" + str(datanumber) + "-type"
            if typekey in bibentry.fields:
                link["type"] = bibentry.fields[typekey]
            else:
                link["type"] = type
            links.append(link)


def addYoutubeDownloads(links, bibentry):
    for videonumber in range(1, 20):
        key = "extra-youtube-download-" + str(videonumber)
        if key in bibentry.fields:
            link = {}
            link["text_long"] = u'Youtube video %s' % str(videonumber)
            link["text_short"] = u'Youtube video %s' % str(videonumber)
            link["url"] = u'https://www.youtube.com/watch?v=%s' % bibentry.fields[key]
            link["type"] = u'youtube'
            link["external"] = True
            link["embed"] = u'https://www.youtube.com/embed/%s?rel=0' % bibentry.fields[key]
            links.append(link)
