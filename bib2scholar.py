#!/usr/bin/env python3
# coding: utf-8

from scholarly import scholarly
import pybtex.database.input.bibtex
import pybtex.database.output.bibtex
from pybtex.richtext import Text
import Levenshtein as lev
import argparse
import json

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Query Google scholar for all entries in a bib file, and output citation counts and other Google scholar metadata to a JSON file.')

    parser.add_argument('-a', '--author', help="author query string for Google scholar")
    parser.add_argument('bibfile', type=argparse.FileType('r'), help="input bib file")
    parser.add_argument('jsonfile', type=argparse.FileType('a+'), help="output json file. writes the scraped data to this file. if the file already exists, tries to read previously scraped data and updates it with newly scraped data.")
    parser.add_argument('--verbose', '-v', action='count', default=0, help="Set level of verbosity.")
    parser.add_argument('--overwrite-larger', action='store_true', help="Overwrite previous citation counts, even if they are now smaller.")
    parser.add_argument('--skip-details', action='store_true', help="Do not query google scholar for additional citation details when they are missing.")

    args = parser.parse_args()
    if args.verbose > 0:
        print('Reading bibtex data from: %s' % args.bibfile.name)
        print('Reading/Writing scholar data to: %s' % args.jsonfile.name)

    # Load the list of publications from bib file
    parser = pybtex.database.input.bibtex.Parser(encoding="UTF-8")
    bib_data = parser.parse_file(args.bibfile.name)

    total_citations = 0

    try:
        if args.verbose > 0:
            print('Trying to read in the json file ...', end='')
        args.jsonfile.seek(0)
        json_entries = json.load(args.jsonfile)
        if args.verbose > 0:
            print(' success! Using previous scholar data as starting point.')
    except ValueError:
        if args.verbose > 0:
            print(' failed! Using empty scholar data as starting point.')
        json_entries = {}
        json_entries['publications'] = {}

    if args.verbose > 0:
        print("Querying Google Scholar for author profile: \"%s\" ..." % args.author)
    search_query = scholarly.search_author(args.author)
    author = scholarly.fill(next(search_query))
    
    if args.verbose > 2:
        print("Full Google scholar author profile data:")
        print(author)
        print('####################\n')

    json_entries['citedby'] = author["citedby"]
    json_entries['citedby5y'] = author["citedby5y"]
    json_entries['hindex'] = author["hindex"]
    json_entries['hindex5y'] = author["hindex5y"]
    json_entries['i10index'] = author["i10index"]
    json_entries['i10index5y'] = author["i10index5y"]
    json_entries['cites_per_year'] = author["cites_per_year"]
    json_entries['id'] = author["scholar_id"]

    for key, bibentry in bib_data.entries.items():

        # skip patents
        if bibentry.type == 'patent':
            continue

        bib_title = Text.from_latex(bibentry.fields['title']).render_as('plaintext')     
        bib_year = bibentry.fields['year']

        if args.verbose > 0:
            print("%s, %s" % (bib_title, bib_year))

        bib_title = bib_title.lower()

        found = False
        for scholar_pub in author['publications']:
            
            scholar_title = scholar_pub['bib']['title'].lower()
            
            # if the title matches and the scholar_pub has citation statistics
            if "num_citations" in scholar_pub and (scholar_title.startswith(bib_title) or lev.ratio(scholar_title, bib_title) > 0.9):

                # if this publication doesn't exist yet in the json file, add an empty record
                if key not in json_entries['publications']:
                    json_entries['publications'][key] = {}
                
                json_pub = json_entries['publications'][key]
                citations = json_pub.get('citations', 0)
                id_citations = json_pub.get('id_citations', None)
                url = json_pub.get('url', None)

                # if the json publication record is incomplete
                if not (url and id_citations) and not args.skip_details:
                    # try to fetch data from Google scholar

                    if args.verbose > 0:
                        print("   querying Google Scholar for cited-by url ...")
                        if args.verbose > 1:
                            print('\nbefore filling:\n', scholar_pub, '\n')
                    
                    scholarly.fill(scholar_pub)
                    
                    if args.verbose > 1:
                        print('\nafter filling:\n', scholar_pub, '\n')
                    
                    id_citations = scholar_pub.get("cites_id", id_citations)
                    if "citedby_url" in scholar_pub:
                        url = f'https://scholar.google.com{scholar_pub["citedby_url"]}'

                if not args.overwrite_larger and scholar_pub["num_citations"] < citations:
                    if args.verbose > 0:
                        print(f'WARNING: Google Scholar is returning fewer citations than before. Previous count: {citations}; Current count: {scholar_pub["num_citations"]}. Keeping previous citation count.')
                else:
                    citations = scholar_pub["num_citations"]

                total_citations += citations
                found = True

                if citations:
                    json_pub["citations"] = citations
                if url:
                    json_pub["url"] = url
                if id_citations:
                    json_pub["id_citations"] = id_citations

                if args.verbose > 0:
                    print(f"   Found: {citations} citations.")
                break

        if not found:
            if args.verbose > 0:
                print("   Found: 0 citations.")

    if args.verbose > 0:
        print(f"Sum of citation counts: {total_citations}")
        print(f"Total citation count: {author['citedby']}")

    args.jsonfile.seek(0)
    args.jsonfile.truncate()
    json.dump(json_entries, args.jsonfile, indent=4, separators=(',', ': '))
