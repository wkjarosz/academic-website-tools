#!/usr/bin/env python3
# coding: utf-8
import pybtex.database.input.bibtex
import pybtex.database.output.bibtex
import argparse
import json

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Sanitize a bib file and optionally add Google scholar information from a JSON file.')

    parser.add_argument('-s', '--scholar', type=argparse.FileType('r'), default=None, help="json file with scholar info")
    parser.add_argument('infile', type=argparse.FileType('r'), help="input bib file")
    parser.add_argument('outfile', type=argparse.FileType('w'), help="output bib file")

    args = parser.parse_args()
    print('Reading Google scholar info from: %s' % (args.scholar.name if args.scholar else 'None'))
    print('Reading bib entries from: %s' % args.infile.name)
    print('Writing to: %s' % args.outfile.name)

    month_names = {
        'jan': '1',
        'feb': '2',
        'mar': '3',
        'apr': '4',
        'may': '5',
        'jun': '6',
        'jul': '7',
        'aug': '8',
        'sep': '9',
        'oct': '10',
        'nov': '11',
        'dec': '12'
    }

    # Load the list of publications from bib file
    parser = pybtex.database.input.bibtex.Parser(encoding="UTF-8", macros=month_names)
    bib_data = parser.parse_file(args.infile.name)
    
    scholar_data = json.load(args.scholar) if args.scholar else {}
    
    # print json.dumps(scholar_data, sort_keys=True, indent=4, separators=(',', ': '))

    if 'publications' in scholar_data:
        for key, bibentry in bib_data.entries.items():

            if key not in scholar_data['publications']:
                continue

            if 'citations' not in scholar_data['publications'][key]:
                continue

            bibentry.fields['extra-scholar-count'] = str(scholar_data['publications'][key]['citations'])

            if 'url' not in scholar_data['publications'][key]:
                continue

            bibentry.fields['extra-scholar-url'] = scholar_data['publications'][key]['url']

    writer = pybtex.database.output.bibtex.Writer(encoding="UTF-8")
    writer.write_file(bib_data, args.outfile.name)
